package e6;

import org.junit.Test;
import static org.junit.Assert.assertEquals;
import e5.Book;
import e4.Author;
import e6.BookWithNAuthors;

public class TestBookWithNAuthors {
		@Test
		public void testToString() {
			Author[] author = new Author[2];
			author[0]=new Author("author","author@yahoo.com",'F');
					
			author[1]=new Author("author1","author1@yahoo.com",'M');
			BookWithNAuthors b  = new BookWithNAuthors("asasa",author,45,78);
			System.out.println(b.toString());
			
		}
		
		@Test
		public void testPrintAuthors() {
			Author[] author = new Author[2];
			author[0]=new Author("author","author@yahoo.com",'F');
					
			author[1]=new Author("author1","author1@yahoo.com",'M');
			BookWithNAuthors b1 = new BookWithNAuthors("booktitle",author,45,78);
			b1.printAuthors();
		
				
	
	}
}
