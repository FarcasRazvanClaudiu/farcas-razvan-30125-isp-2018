package e6;

import e4.Author;

public class BookWithNAuthors {

	private String name;
	private Author[] authors;
	private double price;
	private int qtyInStock;
	
	public BookWithNAuthors() {
		this.name="Aventurile";
		authors[0]=new Author("Farcas","farcas@gmail.com",'M');
		authors[1]=new Author("Razvan","razvan@gmail.com",'M');
		this.price= 10.4;
		this.qtyInStock=50;
		
		
		
	}
	
	public BookWithNAuthors(String nam,Author[] auth,double pri,int qty) {
		this.name=nam;
		this.authors=auth;
		this.price= pri;
		this.qtyInStock=qty;
		
		
		
	}
	
	public String getName() {
		return this.name;
	}
	
	public Author[] getAuthors() {
		return this.authors;
	}
	public double getPrice() {
		return this.price;
	}
	
	public void setPrice(double pri) {
		this.price=pri;
	}
	
	public int getQtyInStock() {
		return this.qtyInStock;
	}
	
	public void setQtyInStock(int qty) {
		this.qtyInStock=qty;
	}
	
	public void printAuthors() {
		for(Author a:authors) {
			System.out.println(a.getName());
		}
	}
	
	public String toString() {
		return " book name " + this.name +" by " + this.authors.length ;	
		}
	
}
