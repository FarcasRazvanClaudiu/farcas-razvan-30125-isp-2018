package e5;

import e4.Author;

public class Book {
	private String name;
	private Author author;
	private double price;
	private int qtyInStock;
	
	public Book() {
		this.name="Aventurile";
		author=new Author("Farcas","farcas@gmail.com",'M');
		this.price= 10.4;
		this.qtyInStock=50;
		
		
		
	}
	
	public Book(String nam,Author auth,double pri,int qty) {
		this.name=nam;
		this.author=auth;
		this.price= pri;
		this.qtyInStock=qty;
		
		
		
	}
	
	public String getName() {
		return this.name;
	}
	
	public Author getAuthor() {
		return this.author;
	}
	public double getPrice() {
		return this.price;
	}
	
	public void setPrice(double pri) {
		this.price=pri;
	}
	
	public int getQtyInStock() {
		return this.qtyInStock;
	}
	
	public void setQtyInStock(int qty) {
		this.qtyInStock=qty;
	}
	
	public String toString() {
		return this.name +"by" + this.author.getName()+ "("+ this.author.getGender() +")"+ " at "+ this.author.getEmail();
	}
	
	
	

}
