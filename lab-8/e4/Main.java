package e4;

public class Main {

	public static void main(String[] args) throws Exception{
        CarFactory f = new CarFactory();

        Car a = f.createCar("Passat",7000);
        Car b = f.createCar("BMW",1500);

        f.saveCar(a,"car1.dat");
        f.saveCar(b,"car2.dat");

        Car x = f.takeCar("car1.dat");
        Car y = f.takeCar("car2.dat");
    }
}
