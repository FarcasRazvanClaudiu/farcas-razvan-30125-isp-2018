package e1;

public class Circle extends Shape{

private double radius;
	
	public Circle() {
		this.radius=1.0;
	}
	
	public Circle(double rad) {
		this.radius=rad;
	}
	
	public Circle(double rad,String color,boolean filled) {
		super(color,filled);
		this.radius=rad;
		
	}
	public double getRadius() {
		return radius;
	}
	public void setRadius(double radius) {
		this.radius=radius;
	}
	 double getArea() {
		return Math.PI*(radius*radius);
	}
	 double getPerimeter() {
		return 2*Math.PI*radius;
	}
	public String toString() {
		super.toString();
		return "A Circle with radius "+getRadius()+", which is a subclass of"+super.toString();
	}
}
