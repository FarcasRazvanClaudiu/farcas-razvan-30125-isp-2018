package e2;

public class ProxyImage implements Image {

	private RealImage realImage;
	   private String fileName;
	   private boolean id;
	 
	   public ProxyImage(String fileName,boolean id){
	      this.fileName = fileName;
	      this.id=id;
	   }
	   
	   
	 
	   @Override
	   public void display() {
	      if(realImage == null){
	         realImage = new RealImage(fileName);
	      }
	      if(id==true) {
	      realImage.display();
	      }
	      else {
	    	  realImage.rotatedImage();	
	    	  } 
	   
	   }
	   
	   @Override
	   public void rotatedImage() {
		   realImage.rotatedImage();
	   }



  
	   
	   
}
