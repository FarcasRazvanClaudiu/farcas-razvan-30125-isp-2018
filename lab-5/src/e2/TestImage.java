package e2;

import org.junit.jupiter.api.Test;

public class TestImage {

	@Test
	public void shouldWork() {
		RealImage i1 = new RealImage("qwe");
		ProxyImage i2 = new ProxyImage("rty",true);
		i1.display();
		i2.display();
		i1.rotatedImage();
		i2.rotatedImage();	
	}
	
	
}
