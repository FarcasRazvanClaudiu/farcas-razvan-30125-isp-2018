package e4;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Main {

	public static void main(String args[]) throws Exception {
        Dictionary dict = new Dictionary();
        char raspuns;
        String linie, explic;
        BufferedReader fluxIn = new BufferedReader(new InputStreamReader(System.in));

        do {
            System.out.println("Meniu");
            System.out.println("a - Adauga cuvant");
            System.out.println("d - Listeaza definitii");
            System.out.println("l - Listeaza cuvinte");
            System.out.println("e - Iesi");

            linie = fluxIn.readLine();
            raspuns = linie.charAt(0);

            switch(raspuns) {
                case 'a': case 'A':
                    System.out.println("Introduceti cuvantul:");
                    linie = fluxIn.readLine();
                    if( linie.length()>1) {
                        System.out.println("Introduceti definitia:");
                        explic = fluxIn.readLine();
                        dict.addWord(new Word(linie), new Definition(explic));
                    }
                    break;
                case 'l': case 'L':
                    System.out.println("Afisare cuvinte:");
                    dict.getAllWords();
                    break;
                case 'd': case 'D':
                    System.out.println("Afisare definitii:");
                    dict.getAllDefinitions();
                    break;

            }
        } while(raspuns!='e' && raspuns!='E');
        System.out.println("Program terminat.");
    }
}


