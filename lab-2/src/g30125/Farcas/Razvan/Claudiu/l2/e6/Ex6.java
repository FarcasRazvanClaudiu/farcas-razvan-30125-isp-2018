package g30125.Farcas.Razvan.Claudiu.l2.e6;
import java.util.Scanner;
public class Ex6 {
	   static void nerecursiv(int n){
	       int s=1;
	       for(int i=1;i<=n;i++)
	           s=i*s;
	       System.out.println("N!= "+s);
	   }
	   static int recursiv(int n){
	       if(n==1)
	           return 1;
	       else
	           return n * recursiv(n-1);
	   }
	   public static void main(String[] args){
	       Scanner in = new Scanner(System.in);
	       System.out.println("Valoarea lui N:");
	       int n=in.nextInt();
	       int m=n;
	       System.out.println("N! calculat nerecursiv: ");
	       nerecursiv(n);
	       int k = recursiv(m);
	       System.out.println("N! calculat recursiv: "+k);
	   }
	}


