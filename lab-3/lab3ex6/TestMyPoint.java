package lab3ex6;

public class TestMyPoint {
	public static void main(String[] args) {
		 /*No argument constructor TEST*/
		 MyPoint p1 = new MyPoint();
		 MyPoint p2 = new MyPoint();
		 
		/*Argument Constructor TEST + Get X and get Y TEST*/
		 p1 = new MyPoint(1,1);
		 System.out.println("Arg.Constructor: "+p1.getX()+","+p1.getY());
		 
		 /*Show Coordinates TEST*/
		 p1.ShowXY();
		 
		 /*Set Coordinates TEST*/
		 p1.setXY(2, 2);
		 p1.ShowXY();
		 
		 /*Set X or Y coordinate TEST*/
		 p2.setX(4);
		 p2.ShowXY();
		 p2.setY(5);
		 p2.ShowXY();
		 
		 /*Distance TEST*/
		 System.out.println("Distance between points is: "+p1.distance(2, 6));
		 
		 /*Overloaded Distance TEST*/
		 System.out.println("Distance between points is: "+p1.distance(p2));
	 }

}
