package lab3ex3;
import becker.robots.*;
public class Lab3Ex3 {

	   public static void main(String[] args)
	   {  
	   	// Set up the initial situation
	   	City prague = new City();
	      Thing parcel = new Thing(prague, 1, 2);
	      Robot karel = new Robot(prague, 1, 1, Direction.EAST);
	 
	      karel.turnLeft();	
			// Direct the robot to the final situation
	      karel.move();
	      karel.move();
	      karel.move();
	      karel.move();
	      karel.move();
	      karel.move();
	      karel.turnLeft();	// start turning back as three turn lefts
	      karel.turnLeft();// finished turning back
	      karel.move();
	      karel.move();
	      karel.move();
	      karel.move();
	      karel.move();
	      karel.move();
	      karel.turnLeft();	// start turning back as three turn lefts
	      karel.turnLeft();// finished turning back
	      
	   }
	} 


